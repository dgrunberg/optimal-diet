#python collections class
#Copyright 2016 Daniel Grunberg
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#This is an OrderedDict based class that allows retrival by index as well
#Does not bother to build the data incrementally, so not super efficient in all applications,
#but use as an OrderedDict, and can then ask for coll.getindex[key] or coll.byindex[index]
#Because OrderedDict, the order and index will be the order keys inserted into collection.
import collections
#use this obj class as placeholder to then assign attributes to, if needed
class Obj: pass
#
class Coll(collections.OrderedDict):
    def __init__(self, *x):
        self.dirty=True
        collections.OrderedDict.__init__(self, x)
        
    def __setitem__(self,index,value):
        self.dirty=True
        collections.OrderedDict.__setitem__(self, index, value)

    def build_data(self):
        #when dirty, rebuild both the arry of indexes and the dict giving index value
        self.ary=[ v for k,v in self.items() ]
        self.dict={k: i   for i,(k,v) in enumerate(self.items()) }
        self.dirty=False
                
    def byindex(self, i):
        #retrieve the item by index value, create an array if dirty
        if self.dirty:
            self.build_data()
        return self.ary[i]
    
    def getindex(self, key):
        #get the index value for a key
        if self.dirty:
            self.build_data()
        return self.dict[key]

    def __delitem__(self, key):
        self.dirty=True
        collections.OrderDict.__delitem__(self, key)
        
    def __repr__(self):
        if self.dirty:
            self.build_data()
        ret = repr(collections.OrderedDict.__repr__(self))+'\n'+repr(self.ary)+'\n'+repr(self.dict)
        return ret
    
