#!/usr/bin/env python
#USDA food database analysis and optimization
#Copyright 2016 Daniel Grunberg
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
'''
Computes Omega-3, Omega-6 and ratios for various oils from the SR nutrient database
Python 3
Review:
Foods are 5 digit code, in FOOD_DES.txt
Nutrients are 3 digit code, in NUTR_DEF.txt
Actual amounts are Food~Nutrient in NUT_DATA.txt

'''
import numpy as np
import sys
import math
import scipy.optimize
import pprint
import argparse
import collections
#Project modules
import collect
#
vinfo=sys.version_info
assert  vinfo.major >= 3 and vinfo.minor >= 6

np.set_printoptions(precision=4,linewidth=85)
pp=pprint.PrettyPrinter(indent=4)
#
parser = argparse.ArgumentParser(
    description="usda food database analysis",
    epilog='''
    options that take lists, like --only or --donot, take a comma separated list of values
    E.g.
    ./optimal-diet.py  --only=02000,02011,03001
''')

parser.add_argument("-d", "--data", help="directory to data files from USDA [default data2018/]", default='data2018')
#parser.add_argument("-n", "--nutr", help="file of nutrient list [default nutr.txt]", default='nutr.txt')
#parser.add_argument("-f", "--foods", help="file of foods id list [default foods.txt]", default='foods.txt')
#parser.add_argument("-m", "--nomax", action="store_true", help="do not apply maximum values", default=False)
#parser.add_argument("-v", "--version", action="version", version="0.1.0")
#parser.add_argument("--max", type=float, help="set maximum servings for any food item", default=None)
#parser.add_argument("-o", "--only", help="only consider foods (IDs) on this list", default=None)
#parser.add_argument("-e", "--excl", help="do not use foods (IDs) on this list", default=None)
#parser.add_argument("-p", "--printf", action='store_true', help="print out foods from foods.txt and exit", default=False)
options = parser.parse_args()
#VARIABLES
#foods: Coll of food_id->Obj() with attributes:
#          name, units, min, max, desired, long_name
#nutr: Coll of nutrient-id ->Obj() with attributes:
#             name, group (food group), units, min, max, desired, long_name

def get_nutr(idx):
    #get nutrient with index idx
    for k,v in nutr.items():
        if v[4]==idx: return k

def get_food(idx):
    #get food index idx
    for k,v in foods.items():
        if v[1]==idx: return v[0]

def pr_dict(d):
    #print dict with keys in order
    for k,v in d.items():
        print("{:<12} {}".format(k,v))

def read_foods(foods_file, food_des_file):
  #read a file of a list of food ids and return a hash of foods: [food-id]->[name, index]
  #reads both the foods_file and (food_des_file) FOOD_DES.txt files
  #respect global variables:
  #dict only (only include these IDs)
  #dict excl (exclude those IDs)
  ret=collect.Coll()
  with open(foods_file) as fp:
    for line in fp:
      #skip comment lines
      #print line
      if line[0]=='#': continue
      ary = line.split()
      food_id=ary[0]
      ret[food_id]=collect.Obj()
  #now go through description file and get the food descriptions and validate food ids
  with open(food_des_file,"rt",encoding='latin-1') as fp:
    count=0
    for line in fp:
      ary=line.split('^')
      food_id=ary[0].strip('~')    # 5 digit number food ID
      food_group=ary[1].strip('~')  # 4 digit number food group
      name=ary[2].strip('~')        # Long name
      short_name=ary[3].strip('~') # Short name
      if food_id in ret:
        ret[food_id].name=name
        ret[food_id].group=food_group
        ret[food_id].short_name=short_name
  #make sure all found
  errs=0
  for k,v in ret.items():
    try:
      v.name
    except AttributeError:
      print('did not find {}'.format(k))
      errs+=1
  if errs > 0:
    raise Exception('Missing {} items in foods'.format(errs))
  return ret
#    
def read_nutr(nutr_list, nutr_def_file):
  #from a list of nutrients, read those in the nutrient definition file to return a Coll (Like OrderedDict)
  #return dict of nutrient-id -> [nutrient-name, long-name, units, min, max, desired, index], where index goes from 0->count
  #NUTR_DEF file, we create the final dict with nutrient-id as the key
  ret=collect.Coll()
  for x in nutr_list:
    ret[x]=collect.Obj()
  #go through NUTR_DEF file and lookup ids for nutrients we care about
  #file=NUTR_DEF file, nutr is OrderedDict returned from read_nutr - an entry for each nutrient
  #returns a dict of 'id' -> short_name of nutrient.  We will use this when we scan NUT_DATA
  #EFFECTS: adds long-name to end of nutr entry
  with open(nutr_def_file,"rt",encoding='latin-1') as fp:
    for line in fp:
      #skip comment lines
      ary=line.split('^')
      nutr_id=ary[0].strip('~')         # Nutr_No
      units=ary[1].strip('~')           # Units
      #Need to convert greek mu into u
      if units[0]=='\xB5': units='u'+units[1:]
      name=ary[2].strip('~')            # Tagname
      long_name=ary[3].strip('~')       #NutrDesc
      if nutr_id in ret:
        #this is one we want
        ob=ret[nutr_id]
        ob.units=units
        ob.name=name
        ob.long_name=long_name
  print('found {} nutrient ids'.format(len(ret)))
  return ret

def read_nutr_data(file, foods, nutr):
  #go through NUT_DATA file and pull out foods and nutrient we care about into a giant matrix Q
  #rows are for each food, columns are for each nutrient
  #file=NUTR_DEF file, nutr is OrderedDict returned from read_nutr - an entry for each nutrient
  ret=dict()
  print('foods has {} items'.format(len(foods)))
  print('nutr has {} items'.format(len(nutr)))
  Q=np.zeros( (len(foods), len(nutr)))
  print('matrix Q has shape:', Q.shape)
  with open(file) as fp:
    for line in fp:
      #skip comment lines
      ary=line.split('^')
      foodid=ary[0].strip('~')
      nutrid=ary[1].strip('~')
      #Do we want this value?
      if (foodid in foods) & (nutrid in nutr):
        f_index=foods.getindex(foodid)
        n_index=nutr.getindex(nutrid)
        value=float(ary[2])
        #print line, 'f_index=',f_index,' n_index=', n_index
        Q[f_index,n_index]=value       
  return Q

#these are food NDB number

foods=read_foods('oils.txt', options.data+'/FOOD_DES.txt')
for k, v in foods.items():
  print(f'{k} {v.name} [{v.group}]')
#These are the omega fatty acid nutrient codes
omega3=['621','629','631','851']
omega6=['672','675','685','853','855']
oils_all=omega3+omega6

#Nutrient is list of nutrients we care about, for reporting, minimum, desired, etc.
nutr=read_nutr(oils_all, options.data+'/NUTR_DEF.txt')
for k, v in nutr.items():
  print(f'{k} [{v.units}] {v.name} long={v.long_name}')

#nutr is an ordered dict of nutrient ids that we care about, mapping to a short name.
#foods is an ordered dict of the foods we care about, mapping to object with name, etc.
#Q is a matrix of a row for each food, a column for each nutrient
Q=read_nutr_data(options.data+'/NUT_DATA.txt', foods, nutr)
pp.pprint(Q)
#Sum up the omega3 and omega6
comps=np.zeros((len(foods),3))
#omega-3s
comps[:,0]=np.sum(Q[:,0:len(omega3)], axis=1)
comps[:,1]=np.sum(Q[:,len(omega3):], axis=1)
comps[:,2]=np.sum(comps[:,0:2],axis=1)
#convert to percentages
comps=comps/100   # 100g sample size
pp.pprint(comps)
print('OIL ==============               percentages om3    om6   ratio')
for i, (k,v) in enumerate(foods.items()):
  o3=comps[i,0]
  o6=comps[i,1]
  ratio=''
  if o3!=0:
    ratio='{:6.1f}'.format(o6/o3)
  print(f'{v.name:60}  {o3:6.2f} {o6:6.2f}   {ratio}')




