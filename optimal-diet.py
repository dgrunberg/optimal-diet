#!/usr/bin/env python
#USDA food database analysis and optimization
#Copyright 2016 Daniel Grunberg
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import numpy as np
import sys
import math
import scipy.optimize
import pprint
import argparse
import collections
#Project modules
import collect
#
np.set_printoptions(precision=2,linewidth=130)
pp=pprint.PrettyPrinter(indent=4)
#
parser = argparse.ArgumentParser(
    description="usda food database analysis",
    epilog='''
    options that take lists, like --only or --donot, take a comma separated list of values
    E.g.
    ./optimal-diet.py  --only=02000,02011,03001
''')

parser.add_argument("-d", "--data", help="directory to data files from USDA [default data/]", default='data2018')
parser.add_argument("-n", "--nutr", help="file of nutrient list [default nutr.txt]", default='nutr.txt')
parser.add_argument("-f", "--foods", help="file of foods id list [default foods.txt]", default='foods.txt')
parser.add_argument("-m", "--nomax", action="store_true", help="do not apply maximum values", default=False)
parser.add_argument("-v", "--version", action="version", version="0.1.0")
parser.add_argument("--max", type=float, help="set maximum servings for any food item", default=None)
parser.add_argument("-o", "--only", help="only consider foods (IDs) on this list", default=None)
parser.add_argument("-e", "--excl", help="do not use foods (IDs) on this list", default=None)
parser.add_argument("-p", "--printf", action='store_true', help="print out foods from foods.txt and exit", default=False)
options = parser.parse_args()
#VARIABLES
#foods: Coll of food_id->Obj() with attributes:
#          name, units, min, max, desired, long_name
#nutr: Coll of nutrient-id ->Obj() with attributes:
#             name, group (food group), units, min, max, desired, long_name

def get_nutr(idx):
    #get nutrient with index idx
    for k,v in nutr.items():
        if v[4]==idx: return k

def get_food(idx):
    #get food index idx
    for k,v in foods.items():
        if v[1]==idx: return v[0]

def pr_dict(d):
    #print dict with keys in order
    for k,v in d.items():
        print("{:<12} {}".format(k,v))

def pr_nutr():
    #print nutr global variable out in order
    print('NUTRIENTS')
    print('   ID  units      min      max    long_name')
    for k,v in nutr.items():
        print(f"{k:>5}   {v.units:>4} {v.min:8.2f} {v.max:8.2f}  {v.name:10} {v.long_name}")

def read_foods(foods_file, food_des_file):
    #read and return a hash of foods: [food-id]->[name, index]
    #reads both the (default named) foods.txt and FOOD_DES.txt files
    #respect global variables:
    #dict only (only include these IDs)
    #dict excl (exclude those IDs)
    ret=collect.Coll()
    with open(foods_file) as fp:
        for line in fp:
            #skip comment lines
            #print line
            if line[0]=='#': continue
            if len(line) < 2: continue
            ary = line.split()
            food_id=ary[0]
            #if len(ary) > 1:
            #    wt=float(ary[1])
            #else:
            wt=1.0
            if only is not None:
                if food_id not in only: continue
            if excl is not None:
                if food_id in excl: continue
            #add any missing values as 0
            ob=collect.Obj()
            ret[food_id]=ob
            ob.weight=wt
    print('Using {} foods from {}'.format(len(ret), foods_file))
    #now go through description file and get the food descriptions and validate food ids
    with open(food_des_file,"rt",encoding='latin-1') as fp:
        count=0
        for line in fp:
            ary=line.split('^')
            food_id=ary[0].strip('~')    # 5 digit number food ID
            food_group=ary[1].strip('~')  # 4 digit number food group
            name=ary[2].strip('~')
            if food_id in ret:
                ret[food_id].name=name
                ret[food_id].group=food_group
    #make sure all found
    errs=0
    for k,v in ret.items():
        try:
            v.name
        except AttributeError:
            print('did not find {}'.format(k))
            errs+=1
    if errs > 0:
        raise Exception('Missing {} items in foods'.format(errs))
    return ret
#    
def read_nutr(nutr_file, nutr_def_file):
    #read in nutrient file and definition file to return a Coll (Like OrderedDict)
    #return dict of nutrient-id -> [nutrient-name, long-name, units, min, max, desired, index], where index goes from 0->count
    #first we create a dict of the items in the nutr.txt file, then when we parse the
    #NUTR_DEF file, we create the final dict with nutrient-id as the key
    tmp=collections.OrderedDict()
    with open(nutr_file) as fp:
        count=0
        for line in fp:
            #skip comment lines
            #print line
            if line[0]=='#': continue
            ary = line.split()
            #add any missing values as 0
            while len(ary)<4:
                ary.append(0)
            name=ary[0]
            #           units,  min,             max
            tmp[name]=[ary[1], float(ary[2]), float(ary[3])]
            count+=1
    #go through NUTR_DEF file and lookup ids for nutrients we care about
    #file=NUTR_DEF file, nutr is OrderedDict returned from read_nutr - an entry for each nutrient
    #returns a dict of 'id' -> short_name of nutrient.  We will use this when we scan NUT_DATA
    #EFFECTS: adds long-name to end of nutr entry
    ret=collect.Coll()
    print('items read from nutrient txt file: {}'.format(len(tmp)))
    with open(nutr_def_file,"rt",encoding='latin-1') as fp:
        for line in fp:
            #skip comment lines
            ary=line.split('^')
            nutr_id=ary[0].strip('~')
            units=ary[1].strip('~')
            #Need to convert greek mu into u
            if units[0]=='\xB5': units='u'+units[1:]
            name=ary[2].strip('~')
            long_name=ary[3].strip('~')
            if name in tmp:
                #this is one we want
                #make sure the units match
                if units != tmp[name][0]:
                    #hopefully another entry will be found that matches requested units
                    continue
                    #print('mismatch of units: found {} vs. requested {}'.format(units, nutr[name_short][0]))
                ob = collect.Obj()   #placeholder
                ret[nutr_id]=ob
                ob.units=units
                ob.min=tmp[name][1]
                ob.max=tmp[name][2]
                ob.name=name
                ob.long_name=long_name
    print('found {} nutrient ids'.format(len(ret)))
    return ret

def read_nutr_data(file, foods, nutr):
    #go through NUT_DATA file and pull out foods and nutrient we care about into a giant matrix Q
    #rows are for each food, columns are for each nutrient
    #file=NUTR_DEF file, nutr is OrderedDict returned from read_nutr - an entry for each nutrient
    ret=dict()
    print('foods has {} items'.format(len(foods)))
    print('nutr has {} items'.format(len(nutr)))
    Q=np.zeros( (len(foods), len(nutr)))
    print('matrix Q has shape:', Q.shape)
    with open(file) as fp:
        for line in fp:
            #skip comment lines
            ary=line.split('^')
            foodid=ary[0].strip('~')
            nutrid=ary[1].strip('~')
            #Do we want this value?
            if (foodid in foods) & (nutrid in nutr):
                f_index=foods.getindex(foodid)
                n_index=nutr.getindex(nutrid)
                value=float(ary[2])
                if nutrid == '208' and value < 0.1:
                    print(f'making nutr {nutrid} for {foodid} 1.0')
                    value = 1.0
                    
                #print line, 'f_index=',f_index,' n_index=', n_index
                Q[f_index,n_index]=value       
    return Q

#These are dicts for restricting ingredient choices
only=None
if options.only is not None:
    only=dict.fromkeys(options.only.split(','),0)
excl=None
if options.excl is not None:
    excl=dict.fromkeys(options.excl.split(','),0)
print('only=',only)
print('excl=',excl)

    

#these are food NDB number

foods=read_foods(options.foods, options.data+'/FOOD_DES.txt')
if options.printf:
    for k, v in foods.items():
        print(f'{k:10} {v.name} {v.weight}')
    sys.exit(0)
    
#Nutrient is list of nutrients we care about, for reporting, minimum, desired, etc.
nutr=read_nutr(options.nutr, options.data+'/NUTR_DEF.txt')
pr_nutr()
#pp.pprint(ids)
#ids is a dict of nutrient ids that we care about, mapping to a short name.  This name can
#be looked up in nutr dict.
#Q is a matrix of a row for each food, a column for each nutrient
Q=read_nutr_data(options.data+'/NUT_DATA.txt', foods, nutr)
#pp.pprint(Q)


print(f'\n Values Q Matrix')
i=0
for k, v in foods.items():
    print(f'{v.name:20.20} cal {Q[i,3]} E {Q[i,10]}')
    i+=1

#Now set up the linear program
#Determine cost function:
how=0
#0=minimize calories
#1=minimize servings
#2=minimize servings times weight in foods file
#3=limit number of serving of any 1 food
bounds=None
if how==0:
    #Minimize calories meeting all required items (ENERC_KCAL0
    z=np.zeros( (len(nutr)))
    #Nutrient-id 208 is ENERC_KCAL, total calories, goal is to minimize that.
    #To change cost function to minimize, change the vector c
    #Q*z is the calories per food item
    z[ nutr.getindex('208') ]=1.0
    c=Q.dot(z)
    #mimimizing just calories in the Q matrix (column corresponding to '208')
elif how==1:
    #Minimize total servings - using this as we are making cals=constant
    c=np.ones( len(foods) )
elif how==2:
    #multiply by the weight provided in the foods file
    c=np.array( [v.weight for v in foods.values()] )
    print(c)
elif how==3:
    #Minimize calories meeting all required items (ENERC_KCAL) and limit # servings
    servings_limit=3  # only 3 servings of any 1 food
    z=np.zeros( (len(nutr)))
    #Nutrient-id 208 is ENERC_KCAL, total calories, goal is to minimize that.
    #To change cost function to minimize, change the vector c
    #Q*z is the calories per food item
    z[ nutr.getindex('208') ]=1.0
    c=Q.dot(z)
    bounds=(0,servings_limit)
else:
  print('unkown how {}'.format(how))
  sys.exit(1)
#Bounds are minimums
#create an array of the mins and maxs:
p=[[v.min, v.max ]   for k,v in nutr.items() ]
#Make an array out of the mins, maxs
P=np.array(p)
#pp.pprint(P)
#Q=[ A1 ; A2 ], where A1 is the max values and A2 is the min values

#m1=mask for max's
#m2=mask for min's
no_max=True
m1=P[:,1]>0
m2=P[:,0]>0
print('we have {} max constraints'.format(m1.sum()))
print('we have {} min constraints'.format(m2.sum()))
A1=Q.T[m1,:]
A2=-Q.T[m2,:]
#create the b1,b2 that will make up b
b1=P[m1,1]
b2=-P[m2,0]
if options.nomax:
    print('not using maximums')
    A=A2
    b=b2
else:
    A=np.append(A1, A2, axis=0)
    b=np.append(b1, b2)
A_eq=None
b_eq=None
#Set up calories, less fiber to be exactly 2000
Desired_cals=2000.0
if how!=0 and how!=3:
    #Force calories to 2000 exactly
    A_eq=(Q.T[nutr.getindex('208'),:]-4.0*Q.T[nutr.getindex('291'),:]).reshape((1,len(foods)))
    b_eq=Desired_cals
#sums=Q.sum(axis=0)
#print 'column sums=', sums
#need tol for it converge (or possible bland=True)
#print('A_ub:', A.shape)
#print('c:', c.shape)
bounds=None
if options.max is not None:
    bounds=(0, options.max)
#Minimizing c'x such that A_ub*x<b_ub, A_eq*x=b_eq
#cals is 2000, minimizing:
ret = scipy.optimize.linprog(c, A_ub=A, b_ub=b, A_eq=A_eq, b_eq=b_eq, options=dict(tol=1e-8, bland=True), bounds=bounds)
print('results success={} status={} message={}'.format(ret.success, ret.status,ret.message))
if not ret.success:
    print('FAILED TO FIND SOLUTION!')
    sys.exit(1)
#PRINT OUT RESULTS    
print('optimal cost = {}'.format(ret.fun))
print('foods in 100g portions')
print ("\nfood id    portions    prot   prot%   food-description")
#print(foods.keys())
protidx = nutr.getindex('203')
calidx = nutr.getindex('208')
for i, (k, v) in enumerate(foods.items()):
    if ret.x[i] > 0:
        protein = Q[i,protidx]*ret.x[i]
        protratio = Q[i,protidx]*4/Q[i,calidx]
        print(f"{k:<10} {ret.x[i]:8.2f} {protein:8.1f} {protratio*100:8.1f}%   {v.name:50.50}")

i=0
#the values of the nutrients
s=Q.T.dot(ret.x)
#Give out the big 3: PROTEIN(203), FAT(204), CARBS(205)
tot=0.0
#first we calculate calories for each nutrient 203/204/205, subtracting grams of fiber from carbs,
#and store these in an array kcals[].  Then we print out.
kcals=list()
grams=list()
for id in ['203', '204', '205']:
    if id=='204':
        cals_per_g=9.0
    else:
        cals_per_g=4.0
    adj=0
    if id=='205': adj=s[nutr.getindex('291')]    #fiber adj
    gg = s[nutr.getindex(id)]-adj
    grams.append(gg)
    c = gg * cals_per_g
    kcals.append(c)
    tot+=c
print('')
print('Calories by category:')
print(f'                                   grams  calories    perc by cal')
cats=['Protein','Fat','Carbs, less fiber']
for i,id in enumerate(['203', '204', '205']):
    print(f'{cats[i]:30}  {grams[i]:8.1f} {kcals[i]:8.0f}  {100*kcals[i]/tot:8.1f}')
print(    '{:30}  {:8.1f}'.format('TOTAL', tot))
print('')
#compute the nutrients for each food in a matrix
print('x={}'.format(ret.x))  # 1 x num_foods
#print(Q.shape)
#Through broadcasting, M is Nutrient matrix weighted by amount of each food used
M=((Q.T)*ret.x).T
#show first 5 nutrients
#print('first 4 nutrients by food M=\n{}'.format(M[:,0:4])) 
#find the largest value in each row and report
print ("nutrient      units       min       max     value name                   major contributor")
i=0
for k,v in nutr.items():
    mi=M[:,i].argmax()  #index of the largest value
    mx=M[:,i].max()     #max value
    f=foods.byindex(mi).name
    print('{} {:<10} {:>4} {:9.2f} {:9.2f} {:9.2f} {:20.20} ({:10.10}:{:8.1f})'.format(
        k,v.name,v.units,v.min,v.max,s[i],v.long_name,f,mx))
    i+=1

#
#Normalize each column of M to total 1:
S=M*(1/M.sum(axis=0))
i = 0
print()
for k, v in foods.items():
    if ret.x[i] > 0:
        print(f'{v.name:20.20} {S[i,:]}')
    i+=1
i = 0
print(f'\n Values Q Matrix')
for k, v in foods.items():
    if ret.x[i] > 0:
        print(f'{v.name:20.20} cal {Q[i,3]} E {Q[i,10]}')
    i+=1

