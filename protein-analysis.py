#!/usr/bin/env python
#USDA food database analysis and optimization
#Copyright 2016 Daniel Grunberg
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import numpy as np
import sys
import os
import math
import scipy.optimize
import pprint
import argparse
import collections
#Project modules
import collect

#
np.set_printoptions(precision=4,linewidth=85)
pp=pprint.PrettyPrinter(indent=4)
#
parser = argparse.ArgumentParser(
    description="usda food database analysis",
    epilog='''
    options that take lists, like --only or --donot, take a comma separated list of values
    E.g.
    ./optimal-diet.py  --only=02000,02011,03001
''')

parser.add_argument("-d", "--data", help="directory to data files from USDA [default data/]", default='data2018')
parser.add_argument("-n", "--nutr", help="file of nutrient list [default nutr.txt]", default='nutr.txt')
parser.add_argument("-f", "--foods", help="file of foods id list [default foods.txt]", default='foods.txt')
parser.add_argument("-m", "--nomax", action="store_true", help="do not apply maximum values", default=False)
parser.add_argument("-v", "--version", action="version", version="0.1.0")
parser.add_argument("--max", type=float, help="set maximum servings for any food item", default=None)
parser.add_argument("-o", "--only", help="only consider foods (IDs) on this list", default=None)
parser.add_argument("-e", "--excl", help="do not use foods (IDs) on this list", default=None)
parser.add_argument("-p", "--printf", action='store_true', help="print out foods from foods.txt and exit", default=False)
options = parser.parse_args()
#VARIABLES
#foods: Coll of food_id->Obj() with attributes:
#          name, units, min, max, desired, long_name
#nutr: Coll of nutrient-id ->Obj() with attributes:
#             name, group (food group), units, min, max, desired, long_name
#This is the list of essential proteins with min mg/kg required for a 800 mg/kg protein diet
NUTR_DEF_FILE = os.path.join(options.data, 'NUTR_DEF.txt')

essential_aminos={ 'LEU_G': 39,
                   'HISTN_G': 10,
                   'ILE_G': 20,
                   'LYS_G': 30,
                   'MET_G': 10.4,
                   'PHE_G': 25,
                   'THR_G': 15,
                   'TRP_G': 4,
                   'VAL_G': 26
            }

#Protein is PROCNT
#cals is ENERC_KCAL
#fat is FAT
nutr_dict = essential_aminos.copy()
#Add in others
nutr_dict['PROCNT']=0
nutr_dict['ENERC_KCAL']=0
nutr_dict['FAT']=0

def get_nutr_id_from_name(nutr, name):
    for k,v in nutr.items():
        if v.name == name:
            #print(f'{v.name=} {name=}')
            return k
    raise ValueError(f'could not find {name}')

def pr_dict(d):
    #print dict with keys in order
    for k,v in d.items():
        print("{:<12} {}".format(k,v))

def pr_nutr():
    #print nutr global variable out in order
    print('NUTRIENTS')
    print('   ID  units      min      max    long_name')
    for k,v in nutr.items():
        print("{:>5}   {:>4} {:8.2f} {:8.2f}   {}".format(k,v.units, v.min, v.max, v.long_name))

def read_foods(foods_file, food_des_file):
    #read and return a hash of foods: [food-id]->[name, index]
    #reads both the (default named) foods.txt and FOOD_DES.txt files
    #respect global variables:
    #dict only (only include these IDs)
    #dict excl (exclude those IDs)
    ret=collect.Coll()
    with open(foods_file) as fp:
        for line in fp:
            #skip comment lines
            print(line)
            if line[0]=='#': continue
            if len(line) < 2: continue
            ary = line.split()
            food_id=ary[0]
            wt=1.0
            if only is not None:
                if food_id not in only: continue
            if excl is not None:
                if food_id in excl: continue
            #add any missing values as 0
            ob=collect.Obj()
            ret[food_id]=ob
            ob.weight=wt
    print('Using {} foods from {}'.format(len(ret), foods_file))
    #now go through description file and get the food descriptions and validate food ids
    with open(food_des_file,"rt",encoding='latin-1') as fp:
        count=0
        for line in fp:
            ary=line.split('^')
            food_id=ary[0].strip('~')    # 5 digit number food ID
            food_group=ary[1].strip('~')  # 4 digit number food group
            name=ary[2].strip('~')
            if food_id in ret:
                ret[food_id].name=name
                ret[food_id].group=food_group
    #make sure all found
    errs=0
    for k,v in ret.items():
        try:
            v.name
        except AttributeError:
            print('did not find {}'.format(k))
            errs+=1
    if errs > 0:
        raise Exception('Missing {} items in foods'.format(errs))
    return ret
#    
def read_nutr(nutr_dict, nutr_def_file):
    #read in nutr definition file to return a Coll (Like OrderedDict)
    #return dict of nutrient-id -> [nutrient-name, long-name, units, min, max, desired, index], where index goes from 0->count
    #NUTR_DEF file, we create the final dict with nutrient-id as the key
    #only look for names (not ids) in nutr_dict
    nutr_to_look_for=nutr_dict
    print(f'looking for the items {nutr_dict}')
    #go through NUTR_DEF file and lookup ids for nutrients we care about
    #file=NUTR_DEF file, nutr is OrderedDict returned from read_nutr - an entry for each nutrient
    #returns a dict of 'id' -> short_name of nutrient.  We will use this when we scan NUT_DATA
    #EFFECTS: adds long-name to end of nutr entry
    ret=collect.Coll()
    with open(nutr_def_file,"rt",encoding='latin-1') as fp:
        for line in fp:
            #skip comment lines
            ary=line.split('^')
            nutr_id=ary[0].strip('~')
            units=ary[1].strip('~')
            #Need to convert greek mu into u
            if units[0]=='\xB5': units='u'+units[1:]
            name=ary[2].strip('~')
            long_name=ary[3].strip('~')
            if name in nutr_to_look_for:
                #this is one we want
                #make sure the units match
                #if units != nutr_to_look_for[name][0]:
                #    #hopefully another entry will be found that matches requested units
                #    continue
                #    #print('mismatch of units: found {} vs. requested {}'.format(units, nutr[name_short][0]))
                ob = collect.Obj()   #placeholder
                ret[nutr_id]=ob
                ob.units=units
                ob.min=0  # not really using here
                ob.max=0  # ditto
                ob.name=name
                ob.long_name=long_name
    print('found {} nutrient ids'.format(len(ret)))
    return ret

def read_nutr_data(file, foods, nutr):
    #go through NUT_DATA file and pull out foods and nutrient we care about into
    #a giant matrix Q, with:
    #row for each food, column for each nutrient
    #file=NUTR_DEF file, nutr is OrderedDict returned from read_nutr - an entry for each nutrient
    ret=dict()
    print('foods has {} items'.format(len(foods)))
    print('nutr has {} items'.format(len(nutr)))
    Q=np.zeros( (len(foods), len(nutr)))
    print('matrix Q has shape:', Q.shape)
    with open(file) as fp:
        for line in fp:
            #skip comment lines
            ary=line.split('^')
            foodid=ary[0].strip('~')
            nutrid=ary[1].strip('~')
            #Do we want this value?
            if (foodid in foods) & (nutrid in nutr):
                f_index=foods.getindex(foodid)
                n_index=nutr.getindex(nutrid)
                value=float(ary[2])
                #print line, 'f_index=',f_index,' n_index=', n_index
                Q[f_index,n_index]=value       
    return Q

#These are dicts for restricting ingredient choices
only=None
if options.only is not None:
    only=dict.fromkeys(options.only.split(','),0)
excl=None
if options.excl is not None:
    excl=dict.fromkeys(options.excl.split(','),0)
print('only=',only)
print('excl=',excl)

nutr=read_nutr(nutr_dict, NUTR_DEF_FILE)
foods=read_foods(options.foods, options.data+'/FOOD_DES.txt')
if options.printf:
    for k, v in foods.items():
        print("{:<10} {}".format(k,v.name))
    sys.exit(0)
    
#Nutrient is list of nutrients we care about, for reporting, minimum, desired, etc.
#ids is a dict of nutrient ids that we care about, mapping to a short name.  This name can
#be looked up in nutr dict.
#Q is a matrix of a row for each food, a column for each nutrient
Q=read_nutr_data(options.data+'/NUT_DATA.txt', foods, nutr)
#pp.pprint(Q)
#compute protein quality
#for each amino acid:
#ratio = (amount in food/total protein) / (amount required / 800 )
#take the smallest percentage over all amino acids
print(f'nutr items')
for k,v in nutr.items():
    print(f'{k=} {v.name=} {v.long_name=}')
r, c = Q.shape
print()
protid = get_nutr_id_from_name(nutr, 'PROCNT')
tot_prot_idx = nutr.getindex(protid)
for i in range(r):
    food = foods.byindex(i)
    ratio_list = []
    print(f'\n{food.name}')
    for essname, req in essential_aminos.items():
        #go through name, required amount
        essid = get_nutr_id_from_name(nutr, essname)
        if essid is None:
            print(f'bad essid')
            sys.exit(1)
        essidx = nutr.getindex(essid)
        ess = nutr[essid]
        essamt = Q[i, essidx]
        tot_prot = Q[i, tot_prot_idx]
        ratio = (essamt/tot_prot)/(req/800)
        ratio_list.append(ratio)
        print(f'{essname:10} {ess.long_name:20} {essid:4} {essidx:3} req {req:8.1f} {ratio:8.4f}')
    min_ratio = min(ratio_list)
    print(f'min ratio for {essname} is {min_ratio}')
    
